Name:           belle-sip
Version:        4.5.15
Release:        1%{?dist}
Summary:        SIP transport implementation (RFC3261)

License:        GPLv3+
URL:            https://github.com/BelledonneCommunications/%{name}
Source0:        https://github.com/BelledonneCommunications/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
# unbundle antlr3c, not intended for upstream
Patch0:         %{name}-antlr3c.patch
# remove rpath, not intended for upstream
Patch1:         %{name}-rpath.patch

BuildRequires:  antlr3-C-devel
BuildRequires:  bctoolbox-devel
BuildRequires:  cmake
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  zlib-devel

%description
Belle-sip is a modern library implementing SIP (RFC3261) transport, transaction
and dialog layers. It is written in C, with an object oriented API.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -p1
rm -rf src/antlr3c

%build
%cmake -DENABLE_STATIC=OFF -DENABLE_TESTS=OFF
%cmake_build


%install
%cmake_install


%{?ldconfig_scriptlets}


%files
%license LICENSE.txt
%doc AUTHORS.md
%doc CHANGELOG.md
%doc README.md
%{_libdir}/libbellesip.so.1

%files devel
%{_includedir}/%{name}/
%{_libdir}/libbellesip.so
%{_libdir}/cmake/BelleSIP/
%{_libdir}/pkgconfig/%{name}.pc


%changelog
